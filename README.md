## 项目说明

兔兔答题知识竞赛活动开源版本，使用THinkPHP6+Golang+Vue.2+Vue.3+TypeScript开发。兔兔答题是一款面向移动端答题的前后端应用程序，可用于考试活动，企业内部考核，内部培训等考试刷题。

## 项目特点

1、前端采用图鸟UI开发，支持编译到多端。支持 微信小程序、公众号H5、APP。

2、后端采用Go和PHP开发，支持高并发、高性能等业务场景。

3、管理端采用Element UI、Vue3和TypeScript，为前端等下流行技术栈。

4、数据库采用MySQL持久化数据存储，Redis作为缓存服务提高系统性能，为当前互联网热门技术栈。


## 仓库地址

数据库链接地址：[](https://pan.quark.cn/s/26f20687e8d8) 提取码：a9aw

代码仓库地址：[https://gitlab.com/tutudati1/tutudati001](https://gitlab.com/tutudati1/tutudati001)

兔兔官网地址：[https://www.tutudati.com/](https://www.tutudati.com/)

兔兔问答社区：[https://support.qq.com/products/620005](https://support.qq.com/products/620005)

插件市场：[https://ext.dcloud.net.cn/plugin?id=16028](https://ext.dcloud.net.cn/plugin?id=16028)

> 由于后端的包比较大，代码托管平台免费版对包大小的上传有限制，需要你添加官方客服获取后端代码地址。添加时备注“兔兔答题”。

## 页面预览

![](./image/tn-dati.jpg)
![](./image/tn-tutu.jpg)
![](./image/tn_author_qrcode.jpg)

## 修改内容

1、修改后端接口域名，找到utils下面的request.js文件，将下面的内容修改为你自己的后端接口地址。
```javascript
 let baseUrl = "http://127.0.0.1:8081/api/v1/";
```

2、修改微信消息订阅ID。将下面的ID配置成你自己微信订阅的消息ID，具体的字段跟自己的需求来，因为后端服务属于你自己搭建的服务，你后端自行处理逻辑。
```javascript
examUpdateTemplateId: "BaWm9rF8yEdHtHd0ICG5QKIyYrDE7GUkrncbCZwYkRE", // 题库更新模板id
rankUpdateTemplateId: "qCm4amH07yZEV9ebLNR5oX_uAdACgHrffnXg7OJVAGA", // 试题排行榜更新
rewardNotifyTemplateId: "tDsclUN505XK3_d3q3cI-25W7NPznaVAC70SRWfeZCc", // 开奖结果通知
appUpdateTemplateId: "_4bOiFIRZG6ltTEEdYZYn2VSfsEse34Sb1Bno4h3SbM", // 软件更新通知
```

3、图片替换。当前代码中所有的静态图片都是使用的兔兔答题的CDN，请替换成你自己的CDN，长久不替换将面临封禁的可能，后续不在单独提供图片资源。

4、关于模板的发型，该模板不允许进行二次售卖、开源等行为，只支持购买者进行二次开发学习和商业行为使用，定期查阅到存在该行为，将面临高额惩罚，请自行关注好这一点。

5、找到manifest.json文件，点击源码模式，将DCloud的appid替换成自己的appid。
```javascript
"name" : "兔兔答题",
"appid" : "",

/* 小程序特有相关 */
"mp-weixin" : {
	"appid" : "微信小程序appid",
	"setting" : {
			"urlCheck" : false,
			"minified" : true,
			"postcss" : true,
			"es6" : true
	},
	"usingComponents" : true,
	"LazyCodeLoading" : true,
	"__usePrivacyCheck__" : true
},
```

## 版权说明

兔兔答题知识竞赛活动开源程序，遵循Apache协议，意味着您无需支付任何费用，也无需授权，即可将TuniaoUI开源版应用到您的产品中，但是需要保留TuniaoUI的信息。